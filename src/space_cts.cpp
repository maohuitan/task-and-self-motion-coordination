#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/PlanningScene.h>
#include <iostream>
#include <fstream>
#include <string>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

#include <Eigen/Dense>
#include <cmath>

//to publish joint states
#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>

//#define withoutCTS
#define withCTS

struct nearest_link_distance {
	double H_star;
	double H_value;
	std::string link_name;
};

//function prototypes
double get_obstacle_link_min_distance(std::string link1, std::string link2);
nearest_link_distance get_H_Hstar();
void bubbleSort(std::vector<double> &A,int arraySize);
double get_deltaHi(std::vector<double> jnt_values, int jnt_number);
void cartesian_straight_trajectory_cts(double pos_goal, 
                                       std::vector<double> joint_values,
                                       ros::Publisher &jnt_pub,
                                       Eigen::MatrixXd deltaE);
double get_deltaHstari(std::vector<double> jnt_values, int jnt_number);    
void self_motion(std::vector<double> &joint_values, ros::Publisher &jnt_pub);                              

//accessing manipulator kinematics (global variables)
const robot_state::JointModelGroup* joint_model_group;
robot_model::RobotModelPtr kinematic_model;
robot_state::RobotStatePtr kinematic_state;
double obstacleX = 0.35;
double obstacleY = 0.0;
double obstacleZ = 0.35;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "space_cts");
	ros::NodeHandle node_handle;
	ros::AsyncSpinner spinner(1);
	spinner.start();
	
	sleep(12.0);
	
	moveit::planning_interface::MoveGroup group("arm");
	//moveit::planning_interface::MoveGroup end_eff("end_effector");
	moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
	//group.setPlannerId("RRTConnectkConfigDefault");
	//group.setPlanningTime(60);
	
	
	ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path",10000, true);
	moveit_msgs::DisplayTrajectory display_trajectory;
	
	
	//to publish joint states
	ros::Publisher joint_pub = node_handle.advertise<sensor_msgs::JointState>("/move_group/fake_controller_joint_states", 1);  
	// or publish under topic "joint_states" then change demo.launch to  <rosparam param="/source_list">[joint_states]</rosparam>

    ROS_INFO("Reference frame of the robot: %s", group.getPlanningFrame().c_str());
	ROS_INFO("Reference frame of the end-effector link: %s", group.getEndEffectorLink().c_str());
	
    //accessing manipulator kinematics
    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    kinematic_model = robot_model_loader.getModel();
    kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));
    joint_model_group = kinematic_model->getJointModelGroup("arm");
    const std::vector<std::string> &joint_names = joint_model_group->getJointModelNames();
	
	std::vector<double> joint_values;  //joint vector
	kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
	
    /* A random configuration Take the manipulator away from the fully stretched out singular configuration */
    joint_values[0] = -1.281158;
    joint_values[1] = 1.6587;
    joint_values[2] = -2.247921;
    joint_values[3] = 0.219933;
    joint_values[4] = 1.160293;
    joint_values[5] = -1.387798;
    joint_values[6] = -0.044969;
  
    kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
	kinematic_state->update();
	
	//add a cuboid
	moveit_msgs::CollisionObject pointObstacle;
    pointObstacle.header.frame_id = group.getPlanningFrame();
    pointObstacle.id = "box1";

    /* Define a box to add to the world. */
    shape_msgs::SolidPrimitive primitive;
    primitive.type = primitive.BOX;
    primitive.dimensions.resize(3);
    primitive.dimensions[0] = 0.02;
    primitive.dimensions[1] = 0.02;
    primitive.dimensions[2] = 0.7;

    /* A pose for the box (specified relative to frame_id) */
    geometry_msgs::Pose box_pose;
    box_pose.orientation.x = 1.0;
    box_pose.position.x =  obstacleX;
    box_pose.position.y =  obstacleY;
    box_pose.position.z =  obstacleZ;

    pointObstacle.primitives.push_back(primitive);
    pointObstacle.primitive_poses.push_back(box_pose);
    pointObstacle.operation = pointObstacle.ADD;
	std::vector<moveit_msgs::CollisionObject> obstacles; 
    obstacles.push_back(pointObstacle); 
    
    ROS_INFO("Add an object into the world");  
    planning_scene_interface.addCollisionObjects(obstacles);
	sleep(2.0);
	
	/* plan first part using ompl */
	geometry_msgs::Pose pose1;
	pose1.orientation.y = 1.0;
	pose1.position.x = 0.5;
	pose1.position.y = 0.1;
	pose1.position.z = 0.5;
	group.setPoseTarget(pose1);
	
	moveit::planning_interface::MoveGroup::Plan my_plan;
    bool success = group.plan(my_plan);
    std::cout << group.getCurrentPose();
    ROS_INFO("Visualizing plan (pose1 goal) %s",success?"":"FAILED");    
    sleep(2.0);

    group.execute(my_plan);
    
    /* bring the model to current pose */
    group.getCurrentState()->copyJointGroupPositions(group.getCurrentState()->getRobotModel()->getJointModelGroup(group.getName()), joint_values);
    for(std::size_t i=0; i < joint_values.size(); ++i)
    {
      ROS_INFO("Initial Joint(IK) %s: %f", joint_names[i+1].c_str(), joint_values[i]);
    }
    
    kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
	kinematic_state->update();
    
    /* here starts CTS implementation */
    self_motion(joint_values, joint_pub);
    ROS_INFO("Self motion executed, sleep for 5 secs");
    sleep(5.0);
    
    double y_goal = -0.3;
    //specify end-effector velocity vector
    Eigen::MatrixXd deltaEE(6,1);
    deltaEE << 0.0,
               -0.0001, 
               0.0,
               0.0,
               0.0,
               0.0;
    cartesian_straight_trajectory_cts(y_goal, joint_values, joint_pub, deltaEE);
    
    ros::shutdown();
    return 0;
}

double get_obstacle_link_min_distance(std::string link1, std::string link2)
{
	const Eigen::Affine3d &ri = kinematic_state->getGlobalLinkTransform(link1);
    const Eigen::Affine3d &riplus1 = kinematic_state->getGlobalLinkTransform(link2);
    
    //~ ROS_INFO_STREAM(link1 << ri.translation());
    //~ ROS_INFO_STREAM(link2 << riplus1.translation());
    
    Eigen::Vector3d rob(obstacleX, obstacleY, obstacleZ*2);
    Eigen::Vector3d Li = riplus1.translation() - ri.translation();
    Eigen::Vector3d pi = rob - ri.translation();
    double lengthi = sqrt(Li(0)*Li(0) + Li(1)*Li(1) + Li(2)*Li(2));
    
    if(lengthi < 0.0001 && lengthi > -0.0001)
    {
		lengthi = 0.01;
	}
	
    double alphai = (Li.dot(pi))/lengthi;
    if (alphai < 0)
    {
		alphai = 0;
	}
	if (alphai > 1)
    {
		alphai = lengthi;
	}
    Eigen::Vector3d ei = Li/lengthi;
    Eigen::Vector3d ci = ri.translation() + alphai*ei;
    Eigen::Vector3d di = ci - rob;
    double distancei = sqrt( (di.transpose()) * di );
    
    //std::cout<< "distance between obstacle and " << link1 << " is: " << distancei << std::endl;
    return distancei;
}

//H* = min(di) H= weighted sum
nearest_link_distance get_H_Hstar()
{
	nearest_link_distance nearLinkDist;
	std::vector<std::string> link_names;
    link_names.push_back("wam/shoulder_yaw_link");   //0
    link_names.push_back("wam/shoulder_pitch_link"); //1 
    link_names.push_back("wam/upper_arm_link");      //2
    link_names.push_back("wam/forearm_link");        //3
    link_names.push_back("wam/wrist_yaw_link");      //4
    link_names.push_back("wam/wrist_pitch_link");    //5
    link_names.push_back("wam/wrist_palm_link");     //6
    
    std::vector<double> distance_vector;
    std::vector<double> unsorted_distance_vector;
    double dist;
    for(int i = 0; i < (link_names.size()-1); ++i)   //note only 6 distances. distance to the last joint is approximately same as to 2nd last joint
    {
		dist = get_obstacle_link_min_distance(link_names[i], link_names[i+1]);
		distance_vector.push_back(dist);
	}
	//~ for(int j=0; j< distance_vector.size(); ++j)
	//~ {
		//~ std::cout << "unsorted" << distance_vector[j] << "\n";
	//~ }
	unsorted_distance_vector = distance_vector;
	bubbleSort(distance_vector, distance_vector.size());
	for(int j=0; j< distance_vector.size(); ++j)
	{
		if ( std::abs(distance_vector[0]-unsorted_distance_vector[j]) < 0.001 )  //std::abs is overloaded
		{
			nearLinkDist.link_name = link_names[j];
			nearLinkDist.H_star = distance_vector[0];
			//std::cout << "nearest link is: " << nearLinkDist.link_name << " and distance is " << nearLinkDist.H_star << std::endl;
			break;
		}
	}
	
	//here calculates H
	double delta = 0.1;
	std::vector<double> beta;
	for (int k = 0; k < distance_vector.size(); ++k)
	{
		beta.push_back( pow (delta, (unsorted_distance_vector[k] - nearLinkDist.H_star) ) );
	}
	double H = 0;
	for(int m = 0; m < unsorted_distance_vector.size(); ++m)
	{
		H += beta[m]*unsorted_distance_vector[m];
	}
	nearLinkDist.H_value = H;
	//std::cout << "H = " << H << std::endl;
	
	return nearLinkDist;
}

void bubbleSort(std::vector<double> &A,int arraySize)
{
    for(int i=0;i<arraySize;i++)
    {
        for(int j=arraySize;j>=i+1;j--)
        {
            if(A[j]<A[j-1])
            {
                double temp = A[j];
                A[j] = A[j-1];
                A[j-1] = temp;
            }
        }
    }
}

double get_deltaHi(std::vector<double> jnt_values, int jnt_number)
{
	nearest_link_distance nld1 = get_H_Hstar();
    
	double deltaThetai = 0.1;  //empirically determined
    jnt_values[jnt_number] = jnt_values[jnt_number] + deltaThetai;
    kinematic_state->setJointGroupPositions(joint_model_group, jnt_values);
	kinematic_state->update();

    nearest_link_distance nld2 = get_H_Hstar();
    double deltaHi = (nld2.H_value - nld1.H_value)/deltaThetai;
    return deltaHi;
}

double get_deltaHstari(std::vector<double> jnt_values, int jnt_number)
{
	nearest_link_distance nld1 = get_H_Hstar();
	
	double deltaThetai = 0.5;  //empirically determined
    jnt_values[jnt_number] = jnt_values[jnt_number] + deltaThetai;
    kinematic_state->setJointGroupPositions(joint_model_group, jnt_values);
	kinematic_state->update();

    nearest_link_distance nld2 = get_H_Hstar();
    double deltaHstari = (nld2.H_star - nld1.H_star)/deltaThetai;
    return deltaHstari;
}

/*instantaneous IK staight line trajectory routine */
void cartesian_straight_trajectory_cts(double pos_goal, 
                                       std::vector<double> joint_values,
                                       ros::Publisher &jnt_pub,
                                       Eigen::MatrixXd deltaE)
{  
  ros::Rate loop_rate(350);
  sensor_msgs::JointState joint_state;
  //make message to publish to fake joint controller
  joint_state.header.stamp = ros::Time::now();
  joint_state.name.resize(7);
  joint_state.position.resize(7);
  joint_state.velocity.resize(7);
  joint_state.name[0] ="wam/base_yaw_joint";
  joint_state.name[1] ="wam/shoulder_pitch_joint";
  joint_state.name[2] ="wam/shoulder_yaw_joint";
  joint_state.name[3] ="wam/elbow_pitch_joint";
  joint_state.name[4] ="wam/wrist_yaw_joint";
  joint_state.name[5] ="wam/wrist_pitch_joint";
  joint_state.name[6] ="wam/palm_yaw_joint";
  
  //to stop while loop 
  double devY = 999.0;
  double y_pos_start;

  //reference point for jacobian calculation  
  Eigen::Vector3d reference_point_position(0.0,0.0,0.0);
  Eigen::MatrixXd jacobian;
            
  Eigen::MatrixXd deltaTheta, pseudoInv;
  
  std::vector<double> joint_velocity_limit;
  joint_velocity_limit.assign(7, 0.2);
  
  double k = 0.01;
  Eigen::MatrixXd deltaH(7,1);
  Eigen::MatrixXd deltaHstar(7,1);
  
  Eigen::MatrixXd eyeMat = Eigen::MatrixXd::Identity(7,7);
  Eigen::MatrixXd N1 = Eigen::MatrixXd::Identity(7,7);
  Eigen::MatrixXd N2 = Eigen::MatrixXd::Identity(7,7);
  Eigen::MatrixXd G1, G2, thetaDotS,thetaDotH, Y1, G2_square, w1prime;
  Eigen::MatrixXd unit(1,1);
  unit << 1.0;
  Eigen::MatrixXd w1(1,1);
  
  Eigen::MatrixXd gammaS, gammaH, gammaTotal; //define obstacle avoidance indicator
  double Hthreshold = 0.3;
  double H_star = 999.0;
  nearest_link_distance getH_star;
  int numWithinTh = 0;
  int timeStep = 0;
  std::vector<std::string> last_current_nearestLinkNames;
  std::string null = " ";
  last_current_nearestLinkNames.push_back(null);
  
  std::ofstream motion_indicator_values;
  motion_indicator_values.open("motion_indicator_values.txt", std::ios::out);
  
  while ( (devY > 0.0001) && (ros::ok()) ){
  timeStep++;
   
  // Get the Jacobian We can also get the Jacobian from the :moveit_core:`RobotState`.
  kinematic_state->getJacobian(joint_model_group, kinematic_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                               reference_point_position,
                               jacobian);

  G1 = N1.inverse() * jacobian.transpose() * (jacobian * N1.inverse() * jacobian.transpose() ).inverse();
  G2 = N2.inverse() * jacobian.transpose() * (jacobian * N2.inverse() * jacobian.transpose() ).inverse();
  G2_square = N2.inverse() * jacobian.transpose() * (jacobian * N2.inverse() * jacobian.transpose() ).inverse();
  Y1 = k*( eyeMat - G2_square * jacobian );
  
  ///*get deltaH and deltaHstar*/
  for(int i = 0; i < 7 ; ++i)
  {
	deltaH(i, 0) = get_deltaHi(joint_values,i);
	deltaHstar(i, 0) = get_deltaHstari(joint_values,i);
	kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
	kinematic_state->update();
  }
    
  kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
  kinematic_state->update();
  //std::cout << "deltaH 7*1 vector: " << deltaH << std::endl;
  //std::cout << "deltaHstar 7*1 vector: " << deltaHstar << std::endl;
  ///*get deltaH and deltaHstar finished*/
  
  getH_star = get_H_Hstar();
  H_star = getH_star.H_star;
  
  thetaDotS = G1*deltaE;
  
  #ifdef withCTS
  if( H_star < Hthreshold ) 
  { 
	numWithinTh++;
	last_current_nearestLinkNames.push_back(getH_star.link_name);
	//std::cout << "current nearest link is " << getH_star.link_name << std::endl;
	
	if ( (( last_current_nearestLinkNames[1].compare(last_current_nearestLinkNames[0]) ) != 0 ) || (numWithinTh == 1) )
	{
		w1prime = -(Y1.row(0) * deltaH) / (Y1(0, 0) * deltaH(0, 0)) + unit;
		std::cout << "w1prime: " << w1prime << "timeStep is "<< timeStep << std::endl;
		if ( w1prime(0,0) > 1.0 )
		{
			if( numWithinTh == 1 )
			{
				w1(0,0) = w1prime(0,0) + 1.0; // make self obs avoidance indicator negative
			}
			else
			{
				//w1(0,0) = w1prime(0,0) - 0.5 * ( std::abs(w1prime(0,0) - 1.0) );  //pos
				w1(0,0) = 1.0;
			}
		}
		else
		{
			if( numWithinTh == 1 )
			{
				//~ double temp = 1.0;                             //plot w1 vs starting indicator value
				//~ while(temp < 200.0){
				//~ w1(0,0) = w1prime(0,0) - temp;  // make self obs avoidance indicator negative
				//~ N2(0, 0) = w1(0,0);
				//~ G2 = N2.inverse() * jacobian.transpose() * (jacobian * N2.inverse() * jacobian.transpose() ).inverse();
				//~ thetaDotH = k*( eyeMat - G2*jacobian )*deltaH;
				//~ gammaH = deltaHstar.transpose() * thetaDotH;
				//~ if (motion_indicator_values.is_open())
				//~ {
					//~ motion_indicator_values << temp << " " << gammaH << "\n" ;
				//~ }
				//~ temp +=1.0;
				//~ }
				//~ motion_indicator_values.close(); 
				w1(0,0) = w1prime(0,0) - 1.0;
			}
			else
			{
				//w1(0,0) = w1prime(0,0) + 0.5 * ( std::abs(w1prime(0,0) - 1.0) ); 
				w1(0,0) = 1.0;
			}
		}
		N2(0, 0) = w1(0,0);
		G2 = N2.inverse() * jacobian.transpose() * (jacobian * N2.inverse() * jacobian.transpose() ).inverse();
		std::cout << "current w1 is " << w1 << std::endl;
	}
	last_current_nearestLinkNames.erase (last_current_nearestLinkNames.begin());
	thetaDotH = k*( eyeMat - G2*jacobian )*deltaH;
  }
  else
  {
	thetaDotH.setZero(7,1); 
  }
  #endif
  
  #ifdef withoutCTS
  thetaDotH = k*( eyeMat - G2*jacobian )*deltaH;
  #endif
  
  if( numWithinTh == 1 )
  {
	Hthreshold = 0.2;
  }
  
  deltaTheta = thetaDotS + thetaDotH;
  gammaS = deltaHstar.transpose() * thetaDotS;
  gammaH = deltaHstar.transpose() * thetaDotH;
  gammaTotal = gammaS + gammaH;
  //std::cout << "task motion obstacle avoidance indicator: " << gammaS << std::endl;
  //std::cout << "self motion obstacle avoidance indicator: " << gammaH << std::endl;
  
  if (motion_indicator_values.is_open())
  {
	 motion_indicator_values << timeStep << " " << gammaS << " " << gammaH << " " << gammaTotal << "\n" ;
  }
  else
  {
	std::cout << "unable to open file motion_indicator_values.txt" << std::endl;
  }

  for(std::size_t j = 0; j < joint_values.size(); ++j)
  {
	  joint_values[j] = joint_values[j] + deltaTheta(j,0);
  }
  
  //set arm configuration
  kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
  kinematic_state->update();
  
  //call forward kinematics
  const Eigen::Affine3d &eef = kinematic_state->getGlobalLinkTransform("wam/wrist_palm_link");
  //sROS_INFO_STREAM("\nTranslation: " << eef.translation());

  y_pos_start = eef(1,3);
  devY = std::abs (y_pos_start - pos_goal);
  
  //make the joint_state message
  joint_state.position[0] = joint_values[0];
  joint_state.velocity[0] = joint_velocity_limit[0];
  joint_state.position[1] = joint_values[1];
  joint_state.velocity[1] = joint_velocity_limit[1];
  joint_state.position[2] = joint_values[2];
  joint_state.velocity[2] = joint_velocity_limit[2];
  joint_state.position[3] = joint_values[3];
  joint_state.velocity[3] = joint_velocity_limit[3];
  joint_state.position[4] = joint_values[4];
  joint_state.velocity[4] = joint_velocity_limit[4];
  joint_state.position[5] = joint_values[5];
  joint_state.velocity[5] = joint_velocity_limit[5];
  joint_state.position[6] = joint_values[6];
  joint_state.velocity[6] = joint_velocity_limit[6];
  
  jnt_pub.publish(joint_state);
  ros::spinOnce();
  loop_rate.sleep();
  }
  motion_indicator_values.close();   
}

void self_motion(std::vector<double> &joint_values, ros::Publisher &jnt_pub)
{  
  std::vector<double> init_jnt_values;
  init_jnt_values.resize (joint_values.size());
  for(std::size_t t = 0; t < init_jnt_values.size(); ++t)
  {
	  init_jnt_values[t] = joint_values[t];
  }
  
  ros::Rate loop_rate(350);
  sensor_msgs::JointState joint_state;
  //make message to publish to fake joint controller
  joint_state.header.stamp = ros::Time::now();
  joint_state.name.resize(7);
  joint_state.position.resize(7);
  joint_state.velocity.resize(7);
  joint_state.name[0] ="wam/base_yaw_joint";
  joint_state.name[1] ="wam/shoulder_pitch_joint";
  joint_state.name[2] ="wam/shoulder_yaw_joint";
  joint_state.name[3] ="wam/elbow_pitch_joint";
  joint_state.name[4] ="wam/wrist_yaw_joint";
  joint_state.name[5] ="wam/wrist_pitch_joint";
  joint_state.name[6] ="wam/palm_yaw_joint";

  //reference point for jacobian calculation  
  Eigen::Vector3d reference_point_position(0.0,0.0,0.0);
  Eigen::MatrixXd eyeMat = Eigen::MatrixXd::Identity(7,7);
  Eigen::MatrixXd jacobian, deltaTheta, nullSpace, pseudoInv, deltaRedun(7,1);
  
  for(std::size_t q = 0; q < init_jnt_values.size(); ++q)
  {
	
	init_jnt_values[q] = -0.001 * init_jnt_values[q] / ( std::abs( init_jnt_values[q] ) );
	deltaRedun(q,0) = init_jnt_values[q];
	if( isnan(deltaRedun(q,0)) )
	{
		deltaRedun(q,0) = 0.000;
	} 
  }
  
  //since initial IK 3rd joint is close to 0, here make sure deltaRedun(2,0) is  -0.001 which is what we need.---> Only true in this target pose
  if( deltaRedun(2,0) > 0)
  {
	deltaRedun(2,0) = deltaRedun(2,0)*(-1.0);
  }
  //std::cout << "deltaRedun: " << deltaRedun << std::endl;
  
  std::vector<double> joint_velocity_limit;
  joint_velocity_limit.assign(7, 0.2);
  
  int counter = 1;
  
  while ( ros::ok() && (counter <= 1000) ){
	 
  kinematic_state->getJacobian(joint_model_group, kinematic_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                               reference_point_position,
                               jacobian);
  
  pseudoInv = (jacobian.transpose())*((jacobian*(jacobian.transpose())).inverse());

  nullSpace = (eyeMat - pseudoInv*jacobian);
  //std::cout << "nullspace: " << nullSpace << std::endl;
  /* update joint values */
  
  deltaTheta = nullSpace*(deltaRedun); 
  //std::cout << "deltaTheta: " << deltaTheta << std::endl;
  for(std::size_t j = 0; j < joint_values.size(); ++j)
  {
	  joint_values[j] = joint_values[j] + deltaTheta(j,0);
	  //std::cout << "new joint_values: " << joint_values[j] << std::endl;
  }
  
  kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
  kinematic_state->update();
  //kinematic_state->enforceBounds();
  
    //make the joint_state message
  joint_state.position[0] = joint_values[0];
  joint_state.velocity[0] = joint_velocity_limit[0];
  joint_state.position[1] = joint_values[1];
  joint_state.velocity[1] = joint_velocity_limit[1];
  joint_state.position[2] = joint_values[2];
  joint_state.velocity[2] = joint_velocity_limit[2];
  joint_state.position[3] = joint_values[3];
  joint_state.velocity[3] = joint_velocity_limit[3];
  joint_state.position[4] = joint_values[4];
  joint_state.velocity[4] = joint_velocity_limit[4];
  joint_state.position[5] = joint_values[5];
  joint_state.velocity[5] = joint_velocity_limit[5];
  joint_state.position[6] = joint_values[6];
  joint_state.velocity[6] = joint_velocity_limit[6];

  jnt_pub.publish(joint_state);
  
  ros::spinOnce();
  loop_rate.sleep();
  counter++;
  }   
}
